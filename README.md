# retroshield1802

There are 3 Arduino Files based on the binary images provided by Chuck (Charles J, Yakym). These are variations of his Membership Card Super Monitor Program, and two BASIC options:

* k1802_MCSMP15_tinyBASIC: contains MCSMP v1.5 and tinyBASIC
* k1802_MCSMP20A_Basic3: contains MCSMP v2.0A and RCA BASIC3 v1.1
* k1802_MCSMP2SP_bothBASICs: contains MCSMP version 2SP and both RCA BASIC3 v1.1 and tinyBASIC. 
  * Please note this ROM image must be located at 0x8000 and RAM lives between 0x0000 - 0x17FF. 
  * Upon reset, Arduino will inject a LBR 0x8000 (C0 80 00) instruction at address 0x0000 to get monitor running.

Other monitor programs in the pipeline:

* k1802_Daves_Mon: Dave's monitor v1.0 Copyright Dave Lang 1984 - I will get this done later on.
* k1802_idiot4: Lee Hart's IDIOT monitor - to be posted later on.

Appropriate software documentation in firmware folder of each version.

All UART comminucation happens over 1802's Q (TXD) and EF3# (RXD) pins by bit-banging. MCSMP detects baud upon reset (you need to hit ENTER). The fastest I could get it to work reliably was 300 cpu cycles/bit. I also added a little bit inter-character delay when sending characters to 1802 because I noticed it needs some time to process the character after receving it. Things are fast enough to be playable but of course a 1802 running at 1MHz will do things much quicker.

Please considering purchasing a Membership Card at Lee Hart's website too:
http://www.sunrise-ev.com/membershipcard.htm
